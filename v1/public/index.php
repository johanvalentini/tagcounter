<!DOCTYPE html>
<html>
<head>
	<title>Tag Counter</title>
	<style>
		body{
			text-align: center;
		}
		.section{

		}
	</style>
	
</head>
<body ng-app="tagCounterModule">
<section class="section" ng-controller="tagCounterController">
	<input type="text" ng-model="newNumberOfTagsToLaunch">
	<button ng-click="setNumberOfTagsToLaunch()">Update</button>
	<h2>message: {{message}}</h2>
	<!-- <h1>Inital: {{initalNumberOfLaunches}}</h1>x -->
	<h1>numberOfLaunches: {{numberOfLaunches}}</h1>
	<!-- <h1>Actual: {{actualNumberOfLaunches}}</h1> -->
	<h1>newNumberOfTagsToLaunch: {{newNumberOfTagsToLaunch}}</h1>
	<h1>numberOfTagsToLaunch: {{numberOfTagsToLaunch}}</h1>
	<h1>modulus numberoftagstolaunch: {{numberOfTagsToLaunch - numberOfTags % numberOfTagsToLaunch}}</h1>
	<h1>numberOfTagsLeftToLaunch: {{numberOfTagsLeftToLaunch}} </h1>
	<h1>numberOfButtonPressesLeft: {{numberOfButtonPressesLeft}}</h1>
	<br>
	<h1>amounttodecrement: {{amounttodecrement}}</h1>

	<h1>numberOfTags: {{numberOfTags}}</h1>
	<h1>newNumberOfTags: {{newNumberOfTags}}</h1>
	
	
	<button ng-click="decrementNumberOfButtonPressesLeft()">Counter Clicked</button>
	<div style="width:{{numberOfTagsLeftToLaunch/numberOfTagsToLaunch*100}}%;height:50px;background-color:#333;"></div>
	<p ng-show="showMessage">{{announcement}}<button ng-click="setCurrentLaunchDone()">Done!</button></p>
</section>

<script type="text/javascript" src="/assets/angular.min.js"></script>
<script type="text/javascript" src="/assets/app.js"></script>
</body>

</html>