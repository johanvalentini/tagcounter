var ls = localStorage;

if (!ls["numberOfLaunches"]) {
    ls["numberOfLaunches"] = 0;
};

var tagCounterModule = angular.module('tagCounterModule', []);

tagCounterModule.controller('tagCounterController', function($scope, $http, $interval) {
    $scope.numberOfTags = 0;
    $scope.newNumberOfTags = 0;
    $scope.showMessage = false;
    $scope.announcement = "";
    $scope.initalNumberOfLaunches = 0;
    // $scope.previousLaunchNumber = 0;
    $scope.numberOfLaunches = ls["numberOfLaunches"];
    $scope.actualNumberOfLaunches = 0;
    $scope.numberOfTagsToLaunch = 50;
    $scope.newNumberOfTagsToLaunch = 40;
    $scope.numberOfTagsLeftToLaunch = 0;
    $scope.lastNumberOfTagsLeftToLaunch = 0;
    $scope.numberOfButtonPressesLeft = 0;
    $scope.amounttodecrement = 0;
    $scope.progress = 0;



    $scope.message = "";
    $scope.getCount = function() {
        $http({
            method: 'GET',
            url: '/gettag.php'
        }).
        success(function(data, status, headers, config) {

            $scope.newNumberOfTags = data;

            // var launches = Math.floor(data / $scope.numberOfTagsToLaunch);


            // if ($scope.initalNumberOfLaunches === 0) {
            //     $scope.initalNumberOfLaunches = launches;
            // }

            // if (launches > $scope.numberOfLaunches) {
            //     $scope.numberOfLaunches = launches;
            // }
            if ($scope.numberOfTags == 0) {
                $scope.numberOfTags = $scope.newNumberOfTags;
            };

            $scope.amounttodecrement = $scope.newNumberOfTags - $scope.numberOfTags;
            $scope.progress = $scope.progress + $scope.amounttodecrement;

            $scope.numberOfTagsLeftToLaunch = $scope.numberOfTagsToLaunch - $scope.progress;
            // $scope.numberOfButtonPressesLeft += $scope.newNumberOfTags - $scope.numberOfTags;


            // var amounttodecrement = $scope.lastNumberOfTagsToLaunch - $scope.numberOfTags % $scope.numberOfTagsToLaunch;

            // $scope.lastNumberOfTagsToLaunch = $scope.numberOfTagsLeftToLaunch;
            // $scope.numberOfTagsLeftToLaunch = $scope.numberOfTags % $scope.numberOfTagsToLaunch
            for (var i = $scope.amounttodecrement; i >= 0; i--) {
                // if ($scope.numberOfTagsLeftToLaunch == 0) {
                //     alert("gogogogo!");
                //     break;
                // };

                if ($scope.readyForLaunch()) {
                    $scope.resetCurrentCountdown();
                    // announce ready for launch
                    $scope.announceLaunch();
                    break;
                };
                $scope.decrementNumberOfTagsLeftToLaunch();

            };
            $scope.numberOfTags = $scope.newNumberOfTags;
            // $scope.numberOfTagsLeftToLaunch
            // if (data % 6 == 0) {
            //     $scope.message = "Go!";
            //     $scope.previousLaunchNumber += 1;
            // } else {
            //     $scope.message = "";
            // }
            // $scope.actualNumberOfLaunches = $scope.numberOfLaunches - $scope.initalNumberOfLaunches;
        }).
        error(function(data, status, headers, config) {
            console.log("data: " + data);
            console.log("status: " + status);
            console.log("headers: " + headers);
            console.log("config: " + config);
        })


    }
    $scope.setNumberOfTagsToLaunch = function() {
        $scope.numberOfTagsToLaunch = $scope.newNumberOfTagsToLaunch;
        ls["numberOfTagsToLaunch"] = $scope.newNumberOfTagsToLaunch;
        $scope.numberOfTagsLeftToLaunch
    }
    $scope.setCurrentLaunchDone = function() {
        // if ($scope.numberOfLaunches >= $scope.numberOfTagsToLaunch) {
        //     alert("launch!");
        //     // $scope.numberOfLaunches = 0;
        // };
        $scope.numberOfLaunches++;

    }
    $scope.decrementNumberOfButtonPressesLeft = function() {
        if ($scope.numberOfButtonPressesLeft > 0) {
            $scope.numberOfButtonPressesLeft--;
        };

    }
    $scope.decrementNumberOfTagsLeftToLaunch = function() {
        if ($scope.numberOfTagsLeftToLaunch > 0) {
            $scope.numberOfTagsLeftToLaunch--;
        };
    }
    $scope.resetCurrentCountdown = function() {
        // $scope.numberOfTagsLeftToLaunch = $scope.numberOfTagsToLaunch;
        $scope.progress = 0;
    }
    $scope.readyForLaunch = function() {
        if ($scope.numberOfTagsLeftToLaunch <= 0) {
            return true;
        } else {
            return false;
        }
    }
    $scope.announceLaunch = function() {
        $scope.showMessage = true;
        $scope.announcement = "Launch!";
    }

    $scope.getCount();
    $interval(function() {
        $scope.getCount();
        // $scope.checkIfReadyForLaunch();
    }, 2000)
});



// tagCounterModule.factory('tagService', function(){

// 	var 
// })