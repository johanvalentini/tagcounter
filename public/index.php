<!DOCTYPE html>
<html>
<head>
	<title>Tag Counter</title>
	<link rel="stylesheet" href="/assets/grid.css">
	<link rel="stylesheet" href="/assets/styles.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">

	
</head>
<body ng-app="tagCounterModule" class="grid">
<!-- <section class="overlaygrid">
	<div class="vbar vbar-1"></div>
	<div class="vbar vbar-2"></div>
	
	<div class="hbar hbar-1"></div>
	<div class="hbar hbar-2"></div>
	
</section> -->
<section class="section row" ng-controller="tagCounterController">
<!-- 	<div class="col-6">
		<p>{{launchMessage}}</p>
	</div> -->
	<div class="col-4">
		<div ng-class="showSplash? 'active' : ''" class="newestimage-backdrop">
			<h2>Upload your</h2>
			<h3>distortion image</h3>
			<h2> <br></h2>
			<h2>on</h2>
			<div class="instagram-logo"><img class="responsive-image" src="/assets/instagram_logo_text.png" alt=""></div>
			<h2> <br></h2>
			<h2>And hashtag it</h2>
			<h2>#Doritoscannon</h2>
		</div>
		<div class="newestimage" style="background-image: url('{{recentMediaUrl}}'); background-size:cover;"></div>
		<!-- <img class=" responsive-image " ng-src="{{recentMediaUrl}}" alt=""> -->
	</div>
	<div class="col-2 sidebar-col">
		<div class="onethirdheight first-box">
			<h3>#{{tagName}}</h3>
			<h2>Launches in</h2>	
		</div>
		<div class="onethirdheight second-box">
			<h1 class="counter">{{counter}}</h1>
			<h3>Hashtags</h3>
		</div>
		<div class="onethirdheight third-box">
			<div><img class="responsive-image instagram-logo-sidebar" src="/assets/instagram_logo_text.png" alt=""></div>
		</div>
		<img class="doritos-spark" src="/assets/doritos-spark.png" alt="Doritos logo">
		<!-- <div class="doritoslogo"></div> -->
		
		<!-- <h2>{{tagCounterDifference}}</h2> -->

	</div>
<!-- 	<div class="messagebox" ng-show="messageShown">
		{{messagetext}}
		<button class="messagebutton" ng-click="hideMessage()">Done</button>
	</div>
	<div class="col-3">
		<p>Target</p>
		<input type="number" min="1" max="999" class="taglimitinput" ng-model="newTagLimit" ng-keydown="$event.keyCode == 13 ? setTagLimit() : null"><button ng-click="setTagLimit()">Update</button>	
	</div>
	<div class="col-3">
		<p>Counters</p>
		<p><button ng-click="resetCounter()">Reset Counters</button></p>

	</div>
	<div class="col-3">
		
		<p>Launches</p>
		<h1> {{numberOfLaunches}}</h1>
		
		<p>Total</p>
		<h1>{{numberOfTags}}</h1>

		

	</div>
	<div class="col-3">
		<p>
			Current Target
		</p>
		<h1>{{tagLimit}}</h1>
		<p>Clicks left</p>
		<button ng-click="decrementCounter()">{{numberOfButtonPressesLeft}}</button>
		
		<div class="progresscontainer">
			<div class="progresscount">{{numberOfButtonPresses}} / {{tagLimit}}</div>
			<div class="progressbar" style="width:{{numberOfButtonPresses/tagLimit*100}}%;"></div>
		</div>
	</div> -->
	
	
	
	

</section>

<script type="text/javascript" src="/assets/angular.min.js"></script>
<script type="text/javascript" src="/assets/app.js"></script>
<script type="text/javascript" src="/assets/fastclick.js"></script>
<script type="application/javascript">
	window.addEventListener('load', function() {
	    FastClick.attach(document.body);
	}, false);
</script>
</body>

</html>