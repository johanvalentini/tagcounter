var ls = localStorage;

if (!ls["tagLimit"]) {
    ls["tagLimit"] = 3;
};
if (!ls["counter"]) {
    ls["counter"] = ls["tagLimit"];
};
if (!ls["lastnumberoftags"]) {
    ls["lastnumberoftags"] = 0;
};

if (!ls["manualcountdetract"]) {
    ls["manualcountdetract"] = 0;
};
var tagCounterModule = angular.module('tagCounterModule', []);

tagCounterModule.controller('tagCounterController', function($scope, $http, $interval) {

    $scope.numberOfTags = 0;
    $scope.recentMediaUrl = "";
    $scope.tagLimit = ls["tagLimit"];
    $scope.counter = ls["counter"]; //$scope.tagLimit;
    $scope.tagCounterDifference = 0;
    $scope.lastNumberOfTags = ls["lastnumberoftags"];
    $scope.tagName = "doritoscannon";
    $scope.launchMessage = "";
    $scope.showSplash = false;

    var isLaunchClearedInterval;

    function timeoutSplash() {
        setTimeout(function() {
            $scope.showSplash = false;
        }, 10000);
    }



    $scope.decrementCounter = function() {
        $scope.counter--;
        // if ($scope.counter <= 0) {
        //     $scope.counter = 0;

        // };
        console.log("decremented count")
        console.log("count: " + $scope.counter)
        console.log("lscount: " + ls["counter"])
        ls["counter"] = $scope.counter;

    }

    $scope.setMessage = function(message) {
        $scope.launchMessage = message;
    }


    $scope.setLaunchStatus = function(state) {
        console.log('/api/?method=setlaunchstatus&status=' + state);
        $http({
            method: 'GET',
            url: '/api/?method=setlaunchstatus&status=' + state
        }).
        success(function(data, status) {
            if (status == 200) {
                console.log("setLaunchStatus: " + data);
                if (data == "launch") {
                    $scope.setMessage = "fyyr!";
                } else {
                    $scope.setMessage = "";
                }


            };
        })
    }


    $scope.resetCounter = function() {
        $scope.tagLimit = ls["tagLimit"];
        $scope.counter = $scope.tagLimit;
        ls["counter"] = $scope.tagLimit;
    }

    $scope.getCount = function() {
        console.log('/api/?method=recent_media_url_and_count&tag=' + $scope.tagName);
        $http({
            method: 'GET',
            url: '/api/?method=recent_media_url_and_count&tag=' + $scope.tagName
        }).
        success(function(data, status, headers, config) {

            if (status == 200) {
                $scope.recentMediaUrl = data.url;
                $scope.numberOfTags = data.count;
                // $scope.tagCounterDifference = ($scope.numberOfTags - ls["lastnumberoftags"]) + ls["manualcountdetract"];
                $scope.tagCounterDifference = $scope.numberOfTags - $scope.lastNumberOfTags;
                console.log("difference: " + $scope.tagCounterDifference + "number:" + $scope.numberOfTags + "last: " + $scope.lastNumberOfTags)
                // console.log("amount to decrement: " + $scope.tagCounterDifference + ls["manualcountdetract"]);
                var difference = parseInt($scope.tagCounterDifference);
                difference += parseInt(ls["manualcountdetract"]);
                console.log("difference: " + difference);
                for (var i = 0; i < (difference); i++) {
                    if ($scope.counter <= 0) {
                        break;
                    } else {
                        $scope.decrementCounter();
                    }

                    // if ($scope.counter > 0) {
                    //     $scope.decrementCounter();
                    // } else {
                    //     console.log("broke from loop");
                    //     break;
                    // }

                };
                ls["manualcountdetract"] = 0;
                if ($scope.counter <= 0 && data.status == "wait") {
                    $scope.setLaunchStatus("launch");
                } else if (data.status == "reset") {
                    $scope.resetCounter();
                    $scope.setLaunchStatus("wait");
                }



                $scope.lastNumberOfTags = data.count;
                ls["lastnumberoftags"] = data.count;

            };


            console.log(data);

        }).
        error(function(data, status, headers, config) {
            console.log("data: " + data);
            console.log("status: " + status);
            console.log("headers: " + headers);
            console.log("config: " + config);
        })


    }



    $scope.getCount();
    $interval(function() {
        $scope.getCount();

    }, 6000)

    $interval(function() {
        $scope.showSplash = true;
        timeoutSplash();
    }, 25000)
});