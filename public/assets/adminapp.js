var ls = localStorage;

if (!ls["tagLimit"]) {
    ls["tagLimit"] = 3;
};

if (!ls["counter"]) {
    ls["counter"] = ls["tagLimit"];
};

if (!ls["manualcountdetract"]) {
    ls["manualcountdetract"] = 0;
};

var tagCounterAdminModule = angular.module('tagCounterAdminModule', []);

tagCounterAdminModule.controller('tagCounterAdminController', function($scope, $http, $interval) {
    $scope.currentImage = "";
    $scope.newTagLimit = 10;
    $scope.tagLimit = ls["tagLimit"];

    $scope.setTagLimit = function(number) {
        $scope.tagLimit = number;
        ls["tagLimit"] = number;
    }

    $scope.submitNewTagLimit = function() {
        $scope.setTagLimit($scope.newTagLimit);
    }


    $scope.submitImage = function() {
        $scope.setLatestApprovedImage($scope.currentImage);
    }

    $scope.getCurrentImageUrl = function() {
        $http({
            method: 'GET',
            url: '/api/?method=getcurrentimageurl'
        }).
        success(function(data, status) {
            console.log("data.url: " + data.url);
            console.log("data.launchstate: " + data.launchstate);
            console.log("data.latestapproved: " + data.latestapproved);
            console.log("status: " + status);
            if (status == 200) {
                $scope.currentImage = data.url;
            };
        })
    }
    $scope.setLatestApprovedImage = function(urlToImage) {
        $http({
            method: 'GET',
            url: '/api/?method=approveimage&urltoimage=' + urlToImage
        }).
        success(function(data, status) {
            console.log("data from post:" + data);
            console.log("status from post:" + status);
        })
    }
    $scope.setLaunchStatus = function(state) {
        console.log('/api/?method=setlaunchstatus&status=' + state);
        $http({
            method: 'GET',
            url: '/api/?method=setlaunchstatus&status=' + state
        }).
        success(function(data, status) {
            if (status == 200) {
                console.log("setLaunchStatus: " + data);
            };
        })
    }

    $scope.decrementManually = function() {
        ls["manualcountdetract"]++;
    }


    $scope.getCurrentImageUrl();
    $interval(function() {
        $scope.getCurrentImageUrl();
    }, 6000)


})