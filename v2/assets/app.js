var ls = localStorage;

if (!ls["numberOfLaunches"]) {
    ls["numberOfLaunches"] = 0;
};

var tagCounterModule = angular.module('tagCounterModule', []);

tagCounterModule.controller('tagCounterController', function($scope, $http, $interval) {

    $scope.numberOfTags = 0;
    $scope.tagLimit = 30;
    $scope.newTagLimit = 20;
    $scope.numberOfButtonPresses = 0;
    $scope.numberOfButtonPressesLeft = 0;
    $scope.numberOfLaunches = ls["numberOfLaunches"];
    $scope.messagetext = "";

    $scope.setTagLimit = function() {
        var tagDifference = $scope.newTagLimit - $scope.tagLimit;
        $scope.tagLimit = $scope.newTagLimit;

        // $scope.numberOfButtonPressesLeft = $scope.numberOfButtonPressesLeft + tagDifference;
        if ($scope.numberOfButtonPressesLeft >= $scope.tagLimit - $scope.numberOfButtonPresses) {
            $scope.numberOfButtonPressesLeft = $scope.tagLimit - $scope.numberOfButtonPresses;
        };
    }

    $scope.incrementCounter = function(newNumberOfTags) {
        if ($scope.numberOfButtonPressesLeft < ($scope.tagLimit - $scope.numberOfButtonPresses)) {
            $scope.numberOfButtonPressesLeft += newNumberOfTags - $scope.numberOfTags;
        }


    }
    $scope.decrementCounter = function() {
        if ($scope.numberOfButtonPressesLeft > 0) {
            $scope.numberOfButtonPressesLeft--;
            $scope.numberOfButtonPresses++;
        }
        if ($scope.numberOfButtonPressesLeft >= $scope.tagLimit) {
            $scope.numberOfButtonPressesLeft = $scope.tagLimit;
        };
        $scope.checkForReachedTagLimit();

    }

    $scope.checkForReachedTagLimit = function() {
        if ($scope.numberOfButtonPresses >= $scope.tagLimit) {
            ls["numberOfLaunches"]++;
            $scope.numberOfLaunches++;
            $scope.resetCounter();
            $scope.showMessage("Launch!");

        };
    }

    $scope.showMessage = function(messagetext) {
        $scope.messagetext = messagetext;
        $scope.messageShown = true;

    }
    $scope.hideMessage = function() {
        $scope.messageShown = false;
        $scope.messagetext = "";
    }

    $scope.resetCounter = function() {
        $scope.numberOfButtonPresses = 0;
        $scope.numberOfButtonPressesLeft = 0;
    }

    $scope.getCount = function() {
        $http({
            method: 'GET',
            url: '/gettag.php'
        }).
        success(function(data, status, headers, config) {
            if ($scope.numberOfTags == 0) {
                $scope.numberOfTags = data;
            };
            $scope.incrementCounter(data);
            $scope.numberOfTags = data;

        }).
        error(function(data, status, headers, config) {
            console.log("data: " + data);
            console.log("status: " + status);
            console.log("headers: " + headers);
            console.log("config: " + config);
        })


    }



    $scope.getCount();
    $interval(function() {
        $scope.getCount();

    }, 2000)
});