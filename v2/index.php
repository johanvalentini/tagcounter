<!DOCTYPE html>
<html>
<head>
	<title>Tag Counter</title>
	<link rel="stylesheet" href="/assets/grid.css">
	<link rel="stylesheet" href="/assets/styles.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	
</head>
<body ng-app="tagCounterModule">
<section class="background">
</section>
<section class="section row" ng-controller="tagCounterController">
	<div class="messagebox" ng-show="messageShown">
		{{messagetext}}
		<button class="messagebutton" ng-click="hideMessage()">Done</button>
	</div>
	<div class="col-3">
		<p>Target</p>
		<input type="number" min="1" max="999" class="taglimitinput" ng-model="newTagLimit" ng-keydown="$event.keyCode == 13 ? setTagLimit() : null"><button ng-click="setTagLimit()">Update</button>	
	</div>
	<div class="col-3">
		<p>Counters</p>
		<p><button ng-click="resetCounter()">Reset Counters</button></p>

	</div>
	<div class="col-3">
		
		<p>Launches</p>
		<h1> {{numberOfLaunches}}</h1>
		<!-- <p>ButtonPresses</p>
		<h1>{{numberOfButtonPresses}}</h1> -->
		<p>Total</p>
		<h1>{{numberOfTags}}</h1>

		

	</div>
	<div class="col-3">
		<p>
			Current Target
		</p>
		<h1>{{tagLimit}}</h1>
		<p>Clicks left</p>
		<button ng-click="decrementCounter()">{{numberOfButtonPressesLeft}}</button>
		
		<div class="progresscontainer">
			<div class="progresscount">{{numberOfButtonPresses}} / {{tagLimit}}</div>
			<div class="progressbar" style="width:{{numberOfButtonPresses/tagLimit*100}}%;"></div>
		</div>
	</div>
	
	
	
	

</section>

<script type="text/javascript" src="/assets/angular.min.js"></script>
<script type="text/javascript" src="/assets/app.js"></script>
<script type="text/javascript" src="/assets/fastclick.js"></script>
<script type="application/javascript">
	window.addEventListener('load', function() {
	    FastClick.attach(document.body);
	}, false);
</script>
</body>

</html>