var ls = localStorage;

if (!ls["numberOfLaunches"]) {
    ls["numberOfLaunches"] = 0;
};

var tagCounterModule = angular.module('tagCounterModule', []);

tagCounterModule.controller('tagCounterController', function($scope, $http, $interval) {

    $scope.numberOfTags = 0;
    $scope.recentMediaUrl = "";
    $scope.tagLimit = 5;
    $scope.counter = $scope.tagLimit;
    $scope.tagCounterDifference = 0;
    $scope.lastNumberOfTags = 0;
    $scope.tagName = "testtagcounter";
    $scope.launchMessage = "";
    var isLaunchClearedInterval;

    $scope.decrementCounter = function() {
        $scope.counter--;
        if ($scope.counter <= 0) {
            $scope.counter = 0;

        };

    }

    $scope.setMessage = function(message) {
        $scope.launchMessage = message;
    }


    $scope.setLaunchStatus = function(state) {
        console.log('/gettag.php?method=setlaunchstatus&status=' + state);
        $http({
            method: 'GET',
            url: '/gettag.php?method=setlaunchstatus&status=' + state
        }).
        success(function(data, status) {
            if (status == 200) {
                console.log("setLaunchStatus: " + data);
                $scope.setMessage = "fyyr!";

            };
        })
    }


    $scope.resetCounter = function() {
        $scope.counter = $scope.tagLimit;
    }

    $scope.getCount = function() {
        console.log('/gettag.php?method=recent_media_url_and_count&tag=' + $scope.tagName);
        $http({
            method: 'GET',
            url: '/gettag.php?method=recent_media_url_and_count&tag=' + $scope.tagName
        }).
        success(function(data, status, headers, config) {

            if (status == 200) {
                $scope.recentMediaUrl = data.url;
                $scope.numberOfTags = data.count;
                $scope.tagCounterDifference = $scope.numberOfTags - $scope.lastNumberOfTags;
                for (var i = 0; i < $scope.tagCounterDifference; i++) {
                    if ($scope.count > 0) {
                        $scope.decrementCounter();
                    } else {
                        // somehow reset counter
                        break;
                    }

                };
                if ($scope.count <= 0) {
                    $scope.setLaunchStatus("launch");
                } else {
                    $scope.setLaunchStatus("wait");
                }

                $scope.lastNumberOfTags = data.count;
            };


            console.log(data);

        }).
        error(function(data, status, headers, config) {
            console.log("data: " + data);
            console.log("status: " + status);
            console.log("headers: " + headers);
            console.log("config: " + config);
        })


    }



    $scope.getCount();
    $interval(function() {
        $scope.getCount();

    }, 5000)
});